package query

import "testing"

func TestCalculateOffset(t *testing.T) {
	v1 := CalculateOffset(1, 5)
	if v1 != 0 {
		t.Error("v1 is wrong")
	}

	v2 := CalculateOffset(10, 2)
	if v2 != 18 {
		t.Error("v2 is wrong")
	}

	v3 := CalculateOffset(4, 25)
	if v3 != 75 {
		t.Error("v3 is wrong")
	}
}
