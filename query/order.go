package query

// Order details for a query
type Order struct {
	Direction Direction
	Field     string
}

// Direction defines the available constants for directional ordering
type Direction string

const (
	DirectionASC  Direction = "ASC"
	DirectionDESC Direction = "DESC"
)
