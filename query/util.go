package query

// CalculateOffset figures out the offset on for the query based on which page is requested
func CalculateOffset(page, limit int) int {
	return (limit * page) - limit
}
