package database

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
)

type DB struct {
	TX   Transaction
	Conn *pgx.Conn
	dsn  string
}

// NewConnection creates a new DB connection to use where needed
func NewConnection(ctx context.Context, dsn string) (DB, error) {
	db := DB{
		dsn: dsn,
	}

	// create a new connection
	conn, err := pgx.Connect(ctx, db.dsn)
	if err != nil {
		return DB{}, fmt.Errorf("failed to get db connection: %w", err)
	}

	db.Conn = conn

	return db, nil
}

func (d *DB) Close() {
	d.Conn.Close(context.Background())
}
