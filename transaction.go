package database

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
)

type Transaction struct {
	Conn      pgx.Tx
	committed bool
}

// Transaction starts a new transaction
func (db *DB) Transaction(ctx context.Context) error {
	tx, err := db.Conn.Begin(ctx)
	if err != nil {
		return err
	}

	db.TX.Conn = tx
	return nil
}

// Commit commits a transaction to save changes to the db
func (t *Transaction) Commit(ctx context.Context) error {
	if err := t.Conn.Commit(ctx); err != nil {
		return fmt.Errorf("failed to commit underlying transaction: %w", err)
	}

	t.committed = true
	return nil
}

// Rollback rolls a failed transaction back to previous state
func (t *Transaction) Rollback(ctx context.Context) error {
	if t.committed {
		return nil
	}

	return t.Conn.Rollback(ctx)
}
