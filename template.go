package database

import (
	"bytes"
	"fmt"
	"text/template"
)

func ExecTemplate(templateStr string, params interface{}) (string, error) {
	tmpl, err := template.New("").Parse(templateStr)
	if err != nil {
		return "", fmt.Errorf("unable to parse template: %w", err)
	}

	buf := &bytes.Buffer{}
	if err = tmpl.Execute(buf, params); err != nil {
		return "", fmt.Errorf("failed to execute template with data '%v': %w", params, err)
	}

	return buf.String(), nil
}
